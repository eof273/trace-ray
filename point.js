/**
 * Point constructor;
 * @param {Number} x
 * @param {Number} y
 * @param {Number} z
 * @constructor
 */
function Point(x = 0, y = 0, z = 0) {
  let _x = x;
  let _y = y;
  let _z = z;

  this.toVector = toVector;
  this.add = add;
  this.sub = sub;

  // <editor-fold desc="getters/setters" defaultstate="collapsed">
  this.x = function (value) {
    return value === undefined ? _x : _x = value;
  };

  this.y = function (value) {
    return value === undefined ? _y : _y = value;
  };

  this.z = function (value) {
    return value === undefined ? _z : _z = value;
  };
  // </editor-fold>

  function toVector() {
    return new Vector({x: _x, y: _y, z: _z});
  }

  function add(point) {
    Point.mustBePoint(point);

    return new Point(_x + point.x(), _y + point.y(), _z + point.z());
  }

  function sub(point) {
    Point.mustBePoint(point);

    return new Point(_x - point.x(), _y - point.y(), _z - point.z());
  }
}

Point.isPoint = function (point) {
  return point instanceof Point;
};

Point.mustBePoint = function (point) {
  if (Point.isPoint(point)) {
    return point;
  } else {
    throw new Error('Argument must be a Point!');
  }
};
