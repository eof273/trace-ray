/**
 * Color constructor;
 * @param HR {String | Number} - hex color ('#000000') or dec red;
 * @param G {Number} - dec green;
 * @param B {Number} - dec blue;
 * @constructor
 */
function Color(HR, G, B) {
  this.rage = rage;
  this.addColor = addColor;
  this.subColor = subColor;
  this.averageColor = averageColor;
  this.toString = this.toHex = toHex;
  this.toArray = toArray;
  this.toObject = toObject;

  let _r, _g, _b, _a;

  if (G !== undefined && B !== undefined) {
    _r = normaliseChannel(Math.round(HR));
    _g = normaliseChannel(Math.round(G));
    _b = normaliseChannel(Math.round(B));
  } else if (HR !== undefined) {
    let colorsArr = Color.hexToRGB(HR);

    _r = colorsArr[0];
    _g = colorsArr[1];
    _b = colorsArr[2];
  } else {
    _r = 0;
    _g = 0;
    _b = 0;
  }
  _a = 255;

  // <editor-fold desc="getters/setters;" defaultstate="collapsed">
  this.r = function (channel) {
    return channel === undefined ? _r : _r = normaliseChannel(channel);
  };

  this.g = function (channel) {
    return channel === undefined ? _g : _g = normaliseChannel(channel);
  };

  this.b = function (channel) {
    return channel === undefined ? _b : _b = normaliseChannel(channel);
  };

  this.a = function (channel) {
    return channel === undefined ? _a : _a = normaliseChannel(channel);
  };
  // </editor-fold>

  /**
   * Change intensity;
   * @param k {Number}
   * @return {Color}
   */
  function rage(k) {
    let r = normaliseChannel(_r * k);
    let g = normaliseChannel(_g * k);
    let b = normaliseChannel(_b * k);

    return new Color(r, g, b);
  }

  // @todo Среднее при сложении цветов: (ch0 + ch1) / 2;

  /**
   * Add colors;
   * @param color {Color}
   * @return {Color}
   */
  function addColor(color) {
    Color.mustBeColor(color);

    let r = normaliseChannel(_r + color.r());
    let g = normaliseChannel(_g + color.g());
    let b = normaliseChannel(_b + color.b());

    return new Color(r, g, b);
  }

  /**
   * Subtract colors;
   * @param color {Color}
   * @return {Color}
   */
  function subColor(color) {
    Color.mustBeColor(color);

    let r = normaliseChannel(_r - color.r());
    let g = normaliseChannel(_g - color.g());
    let b = normaliseChannel(_b - color.b());

    return new Color(r, g, b);
  }

  /**
   * Average color function;
   * @param {Color} color
   * @returns {Color}
   */
  function averageColor(color) {
    Color.mustBeColor(color);

    return new Color(
      (_r + color.r()) / 2,
      (_g + color.g()) / 2,
      (_b + color.b()) / 2
    );
  }

  // @todo to static;
  /**
   * Normalise channel function;
   * @param channel {Number}
   * @return {number} - [0; 255]
   */
  function normaliseChannel(channel) {
    return channel > 255 ? 255 : (channel < 0 ? 0 : channel);
  }

  // <editor-fold desc="Convert to other..." defaultstate="collapsed">
  function toHex() {
    return `#${leadingZero(_r, 2)}${leadingZero(_g, 2)}${leadingZero(_b, 2)}`;
  }

  function toArray() {
    return [_r, _g, _b, _a];
  }

  function toObject() {
    return {
      r: _r,
      g: _g,
      b: _b,
      a: _a
    };
  }
  // </editor-fold>

  /**
   * Leading zero function;
   * @param n {Number} - number;
   * @param c {Number} - count of symbols for output;
   * @param b {Number} - base [2; 36];
   * @returns {String}
   */
  function leadingZero(n, c, b = 16) {
    const s = n.toString(b);
    const l = s.length;
    return `${c > l ? '0'.repeat(c - l) : ''}${s}`;
  }
}

Color.isColor = function (color) {
  return color instanceof Color;
};

Color.mustBeColor = function (color) {
  if (Color.isColor(color)) {
    return color;
  } else {
    throw new Error('Argument must be instance of Color!');
  }
};

/**
 * Convert hex string to GRB array;
 * @param hexColor {String} - String in '#000000' format;
 * @return {Array}
 */
Color.hexToRGB = function (hexColor) {
  return [
    normaliseChannel(parseInt(hexColor.substr(1, 2), 16)),
    normaliseChannel(parseInt(hexColor.substr(3, 2), 16)),
    normaliseChannel(parseInt(hexColor.substr(5, 2), 16)),
    255
  ];
};

// <editor-fold desc="Color constants constructors;" defaultstate="collapsed">
/**
 * new Color(255, 0, 0);
 * @return {Color}
 */
Color.Red = (function () {
  return new Color(255, 0, 0);
})();

/**
 * new Color(0, 255, 0);
 * @return {Color}
 */
Color.Green = (function () {
  return new Color(0, 255, 0);
})();

/**
 * new Color(0, 0, 255);
 * @return {Color}
 */
Color.Blue = (function () {
  return new Color(0, 0, 255);
})();

/**
 * new Color(255, 255, 0);
 * @return {Color}
 */
Color.Yellow = (function () {
  return new Color(255, 255, 0);
})();

/**
 * new Color(255, 0, 255);
 * @return {Color}
 */
Color.Magenta = (function () {
  return new Color(255, 0, 255);
})();

/**
 * new Color(0, 255, 255);
 * @return {Color}
 */
Color.Cyan = (function () {
  return new Color(0, 255, 255);
})();

/**
 * new Color(255, 255, 255);
 * @return {Color}
 */
Color.White = (function () {
  return new Color(255, 255, 255);
})();

/**
 * new Color(0, 0, 0);
 * @return {Color}
 */
Color.Black = (function () {
  return new Color();
})();
// </editor-fold>
