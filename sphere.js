/* global Point, Color */

/**
 * Sphere constructor;
 * @param {Point} center
 * @param {Number} radius
 * @param {Color} color
 * @param {Number} specular
 * @param {Number} reflective
 * @constructor
 */
function Sphere(center = new Point(), radius = 1, color = new Color(), specular = 0, reflective = 0) {
  let _center = Point.mustBePoint(center);
  let _radius = radius;
  let _color = Color.mustBeColor(color);
  let _specular = specular;
  let _reflective = normaliseReflective(reflective);

  // <editor-fold desc="getters/setters;" defaultstate="collapsed">
  this.center = function (value) {
    return value === undefined ? _center : _center = Point.mustBePoint(value);
  };

  this.radius = function (value) {
    return value === undefined ? _radius : _radius = value;
  };

  this.color = function (value) {
    return value === undefined ? _color : _color = Color.mustBeColor(value);
  };

  this.specular = function (value) {
    return value === undefined ? _specular : _specular = value;
  };

  this.reflective = function (value) {
    return value === undefined ? _reflective : _reflective = normaliseReflective(value);
  };
  // </editor-fold>

  function normaliseReflective(reflective) {
    return reflective > 1 ? 1 : (reflective < 0 ? 0 : reflective);
  }
}
