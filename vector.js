/* global Infinity */

// Точный режим - хранить оригинальные коорд, и пересчитывать итоговые операции

// x = r * sin(theta) * cos(phi)
// y = r * sin(theta) * sin(phi)
// z = r * cos(theta)

// r = sqrt(x*x + y*y + z*z)
// theta = acos(z / (sqrt(x*x + y*y + z*z))) || atan(sqrt(x*x + y*y) / z)
// phi = atan(y / x)

function Vector(data) {
  // @todo static
  // Порядок округления;
  const EXP = 2;
  const RAD = Math.PI / 180;

  // Декартовы
  let _x = data.x;
  let _y = data.y;
  let _z = data.z;

  // Сферические
  let _r = data.r;

  /**
   * Наклон; (grad)
   * @type {Number}
   */

  let _theta = data.theta;
  /**
   * Строны; (grad)
   * @type {Number}
   */
  let _phi = data.phi;

  if (_r !== undefined && _theta !== undefined && _phi !== undefined) {
    const calcX = round(_r * Math.sin(_theta * RAD) * Math.cos(_phi * RAD));
    const calcY = round(_r * Math.sin(_theta * RAD) * Math.sin(_phi * RAD));
    const calcZ = round(_r * Math.cos(_theta * RAD));

    // @todo Check calculated and entered values by errors;
    // @todo Updated related values after use setters;
    _x = _x || calcX;
    _y = _y || calcY;
    _z = _z || calcZ;
  } else if (_x !== undefined && _y !== undefined && _z !== undefined) {
    const calcR = round(Math.hypot(_x, _y, _z));
    const calcTheta = round((_r ? Math.acos(_z / _r) : Math.atan(Math.hypot(_x, _y) / _z)) / RAD);
    const calcPhi = round(Math.atan(_y / _x) / RAD);

     _r = _r || calcR;
     _phi = _phi || calcPhi;
     _theta = _theta || calcTheta;
  }

  function calculateDecFromSph() {
    const calcX = round(_r * Math.sin(_theta * RAD) * Math.cos(_phi * RAD));
    const calcY = round(_r * Math.sin(_theta * RAD) * Math.sin(_phi * RAD));
    const calcZ = round(_r * Math.cos(_theta * RAD));

    _x = calcX;
    _y = calcY;
    _z = calcZ;

    return true;
  }

  // <editor-fold desc="Декартовы;" defaultstate="collapsed">
  this.x = function (value) {
    return value === undefined ? _x : _x = value;
  };

  this.y = function (value) {
    return value === undefined ? _y : _y = value;
  };

  this.z = function (value) {
    return value === undefined ? _z : _z = value;
  };

  this.getDec = function () {
    return {
      x: _x,
      y: _y,
      z: _z
    };
  };
  // </editor-fold>

  // <editor-fold desc="Сферические" defaultstate="collapsed">
  this.r = function (value) {
    return value === undefined ? _r : _r = value;
  };

  this.theta = function (value) {
    return value === undefined ? _theta : _theta = value;
  };

  this.phi = function (value) {
     return value === undefined ? _phi : _phi = value;
  };

  this.getSph = function () {
    return {
      r: _r,
      theta: _theta,
      phi: _phi
    };
  };
  // </editor-fold>

  //Операции над векторами;
  this.add = function (v) {
    Vector.mustBeVector(v);

    // @todo check to calculated;
    // if (!_x || !_y || !_z) this.getDec();
    // if (!v.x || !v.y || !v.z) v.getDec();

    return new Vector({
      x: _x + v.x(),
      y: _y + v.y(),
      z: _z + v.z()
    });
  };

  this.sub = function (v) {
    Vector.mustBeVector(v);

    // @todo check to calculated;
    // if (!_x || !_y || !_z) this.getDec();
    // if (!v.x || !v.y || !v.z) v.getDec();

    return new Vector({
      x: _x - v.x(),
      y: _y - v.y(),
      z: _z - v.z()
    });
  };

  /**
   * Dot (scalar) product;
   * @param {Vector} v
   * @returns {Number}
   */
  this.dot = function (v) {
    Vector.mustBeVector(v);

    return (_x * v.x()) + (_y * v.y()) + (_z * v.z());
  };

  /**
   * Multiply Vector by Number;
   * @param {Number} n
   * @returns {Vector}
   */
  this.mul = function (n) {
    if (isNaN(n)) {
      throw new Error('Argument for Vector.mul() must be a Number!');
    }

    // @todo check if calculated;
    // if (!_x || !_y || !_z) this.getDec();

    return new Vector({
      x: (_x * n),
      y: (_y * n),
      z: (_z * n)
    });

    // @todo multiply Vector by Vector;
    /*
    if (Vector.isVector(v)) {
      if (!v.x || !v.y || !v.z) v.getDec();

      return new Vector({
        x: (_y * v.getZ() - _z* v.getY()),
        y: (_z * v.getX() - _x* v.getZ()),
        z: (_x * v.getY() - _y* v.getX())
      });
    }
    */
  };

  this.div = function (n) {
    // @todo divide by vector;

    if (isNaN(n)) {
      throw new Error('Argument for Vector.div() must be a Number!');
    }

    if (n === 0) {
      // Сохраняем углы для бесконечного вектора;
      return new Vector({
        x: Infinity,
        y: Infinity,
        z: Infinity,
        r: Infinity,
        theta: _theta,
        phi: _phi
      });
    } else {
      return new Vector({
        x: (_x / n),
        y: (_y / n),
        z: (_z / n)
      });
    }
  };

  this.toPoint = function () {
    return new Point(_x, _y, _z);
  };

  /**
   * Invert vector (v -> -v);<br>
   * Or you can use: (new Vector({x: 0, y: 0, z: 0})).sub(v) :D
   * @returns {Vector}
   */
  this.invert = function () {
    return new Vector({
      x: -_x,
      y: -_y,
      z: -_z
    });
  };

  this.mulMx = function (matrix) {
    const res = [0, 0, 0];
    const v = [_x, _y, _z];

    // matrix.length must be 3 (for x, y, z);
    for(let i = 0, l0 = matrix.length; i < l0; i++) {
      for(let j = 0, l1 = v.length; j < l1; j++) {
        res[i] += matrix[i][j] * v[j];
      }
    }

    return new Vector({
      x: res[0],
      y: res[1],
      z: res[2]
    });
  };

  /**
   * Rotate camera vector;
   * @param {String} axis - 'x', 'y' or 'z'
   * @param {Number} grad - Rotation angle in grad;
   * @returns {Vector}
   */
  this.rotate = function (axis, grad) {
    if (['x','y','z'].indexOf(axis) === -1) throw new Error('First argument must be an axis name (x, y or z)!');

    const cos = +Math.cos(grad * RAD).toFixed(4);
    const sin = +Math.sin(grad * RAD).toFixed(4);

    let matrix;
    switch(axis) {
      case 'x':
        matrix = [
          [1, 0, 0],
          [0, cos, -sin],
          [0, sin, cos]
        ];
        break;
      case 'y':
        matrix = [
          [cos, 0, sin],
          [0, 1, 0],
          [-sin, 0, cos]
        ];
        break;
      case 'z':
        matrix = [
          [cos, -sin, 0],
          [sin, cos, 0],
          [0, 0, 1]
        ];
        break;
    }

    return new Vector({x: _x, y: _y, z: _z}).mulMx(matrix);
  };

  function round(val) {
    return +val.toFixed(EXP);
  }

  function isInfinity(v) {
    // @todo check to undefined;
    return (!isFinite(v.x()) || !isFinite(v.y()) || !isFinite(v.z()) || !isFinite(v.r()));
  }
}

Vector.isVector = function (v) {
  return v instanceof Vector;
};

Vector.mustBeVector = function (v) {
  if (Vector.isVector(v)) {
    return v;
  } else {
    throw new Error('Argument must be a Vector!');
  }
};
