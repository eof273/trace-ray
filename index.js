/*
  P = O + t(V - O) -> (V - O) = →D -> P = O + t→D;

  sphere:
    distance(P, C) = r;
    |P - C| = r;
    sqrt(<P - C, P - C>) = r;
    <P - C, P - C> = r^2;

    <O + t→D - C, O + t→D - C> = r^2;
    →OC = O - C;
    <→OC + t→D, →OC + t→D> = r^2; - Эта поебень дистрибутивна;
    <→OC, →OC> + <t→D, →OC> + <t→D, →OC> + <t→D, t→D> = r^2;
    <t→D, t→D> + 2<t→D, →OC> + <→OC, →OC> = r^2;
    (t^2)<→D, →D> + 2t<→D, →OC> + <→OC, →OC> - r^2 = 0;
    k1 = <→D, →D>;
    k2 = 2<→D, →OC>;
    k3 = <→OC, →OC> - r^2;
    (t^2)k1 + tk2 + k3 = 0;

    t1/2 = (-k2 +- sqrt(k2^2 - 4k1*k3))/2k1;
 */

// @todo Change add, sub... to addVector/Point, subVector/Point etc in ALL constructors;
const scale = 1 / 40;
const canvas = document.getElementById('canvas');
let ctx = canvas.getContext('2d');
/**
 * Canvas width;
 * @type {Number}
 */
let Cw = canvas.width;
/**
 * Canvas height;
 * @type {Number}
 */
let Ch = canvas.height;
/**
 * Viewport width;
 * @type {number}
 */
let Vw = Cw * scale;
/**
 * Viewport height;
 * @type {number}
 */
let Vh = Cw * scale;
/**
 * Canvas image data;
 * @type {ImageData}
 */
let imageData = ctx.createImageData(Cw, Ch);

/**
 * Background color;
 * @type {Color}
 */
const BACKGROUND_COLOR = new Color(80, 180, 200) // (180, 180, 200); // (80, 180, 200);
  .averageColor(Color.Black);

/**
 * Recursion depth for reflection calculate;
 * @type {Number}
 */
const RECURSION_DEPTH = 5;

/**
 * Min t form where we start calculate effects (shadow, reflect etc);
 * @type {Number}
 */
const EPSILON = 0.001;

/**
 * Rotation coefficient. Fucking magick - I really dont understand how it works;
 * @type {Number}
 */
const rotate = 1; // -0.7071; // 0.7071;

/** @type {Scene} */
const scene = {
  camera: {
    position: new Point(0,0,-20)
  },
  spheres: [
    new Sphere(new Point(-120, 45, 130), 20, Color.Magenta.rage(0.5), 50, 0.1),
    new Sphere(new Point(0, -10, 30), 10, Color.Red, 500, 0.2),
    new Sphere(new Point(20, 0, 40), 10, Color.Blue, 500, 0.85),
    new Sphere(new Point(-20, 0, 40), 10, Color.Green, 10, 0.4),
    new Sphere(new Point(0, -5001, 0), 5000 - 7, Color.Yellow, 1000, 0.5)
  ],
  lights: [
    new Light(0.2, Light.AMBIENT),
    new Light(0.6, Light.POINT, new Point(10, 40, -40)),
    new Light(0.2, Light.DIRECTIONAL, new Point(10, 40, 90))
  ]
};

for (let x = -Cw / 2, lastX = Cw / 2; x < lastX; x++) {
  for (let y = -Ch / 2, lastY = Ch / 2; y < lastY; y++) {
    const D = canvasToViewPort(x, y)
      .rotate('x', 10)
      .rotate('y', 10)
      .rotate('z', 10);
    const color = traceRay(scene.camera.position, D, 1, Infinity, RECURSION_DEPTH);
    putPixel(x, y, color);
  }
}
ctx.putImageData(imageData, 0, 0);

// Указываем в 'человеческих' координатах;
function putPixel(x, y, color) {
  let pixelIndex = (Sy(y) * Cw + Sx(x)) * 4;

  imageData.data[pixelIndex + 0] = color.r();
  imageData.data[pixelIndex + 1] = color.g();
  imageData.data[pixelIndex + 2] = color.b();
  imageData.data[pixelIndex + 3] = color.a();
}

function Sx(Cx) {
  return (Cw / 2) + Cx;
}

function Sy(Cy) {
  return (Ch / 2) - Cy;
}

function Vx(Cx) {
  return Cx * Vw / Cw;
}

function Vy(Cy) {
  return Cy * Vh / Ch;
}

function Vz(d = 10) {
  return d;
}

/**
 * From canvas to viewport;
 * @param {Number} x
 * @param {Number} y
 * @returns {Vector}
 */
function canvasToViewPort(x, y) {
  return new Vector({x: Vx(x), y: Vy(y), z: Vz()});
}

/**
 * Trace ray function;
 * @param {Point} O - Camera position;
 * @param {Vector} D - Vector from camera to viewport pixel;
 * @param {Number} tMin - Where we start trace;
 * @param {Number} tMax - Where we finish trace;
 * @param {Number} recursionDepth - Recursion depth for reflection calculate;
 * @returns {Color}
 */
function traceRay(O, D, tMin, tMax, recursionDepth = 0) {
  const closestIntersectionData = closestIntersection(O, D, tMin, tMax);
  let closestT = closestIntersectionData.t;
  let closestSphere = closestIntersectionData.sphere;

  if (closestSphere) {
    const P = O.toVector().add(D.mul(closestT)); // ???
    /**
     * Normal Vector;
     * @type {Vector}
     */
    let N = P.sub(closestSphere.center().toVector());
    N = N.div(N.r());

    /**
     * Local color;
     * @type {Color}
     */
    let localColor = closestSphere.color().rage(
      computeLighting(
        P,
        N,
        D.invert(), // -D;
        closestSphere.specular()
      )
    );

    /**
     * Reflective for closest object;
     * @type {Number}
     */
    let reflective = closestSphere.reflective();
    if (recursionDepth <= 0 || reflective <= 0) {
      return localColor;
    } else {
      /**
       * Reflect ray;
       * @type {Vector}
       */
      let R = reflectRay(D.invert(), N);
      /**
       * Reflected color;
       * @type {Color}
       */
      let reflectedColor = traceRay(P.toPoint(), R, EPSILON, tMax, recursionDepth - 1);
      return localColor.rage(1 - reflective).addColor(reflectedColor.rage(reflective));
    }

  } else {
    return BACKGROUND_COLOR;
  }
}

/**
 * Closest intersection function;
 * @param {Point} O
 * @param {Vector} D
 * @param {Number} tMin
 * @param {Number} tMax
 * @returns {ClosestIntersection}
 */
function closestIntersection(O, D, tMin, tMax) {
  let t = Infinity;
  let sphere = null;

  for (let i = 0, l = scene.spheres.length; i < l; i++) {
    /** @type {Sphere} */
    const s = scene.spheres[i];
    const intersect = intersectRaySphere(O, D, s);
    const t1 = intersect[0];
    const t2 = intersect[1];

    if (tMin <= t1 && t1 <= tMax && t1 < t) {
      t = t1;
      sphere = s;
    }

    if (tMin <= t2 && t2 <= tMax && t2 < t) {
      t = t2;
      sphere = s;
    }
  }

  return {sphere, t};
}

/**
 *
 * @param {Point} O
 * @param {Vector} D
 * @param {Sphere} sphere
 * @returns {Array}
 */
function intersectRaySphere(O, D, sphere) {
  /** @type {Point} */
  const C = sphere.center();
  /** @type {Number} */
  const r = sphere.radius();
  /** @type {Vector} */
  const OC = O.sub(C).toVector(); // O - C;

  const k1 = D.dot(D);
  const k2 = 2 * D.dot(OC);
  const k3 = OC.dot(OC) - (r * r);

  const discriminant = k2 * k2 - 4 * k1 * k3;

  if (discriminant < 0) {
    return [Infinity, Infinity];
  } else {
    const t1 = (-k2 + Math.sqrt(discriminant)) / (2 * k1);
    const t2 = (-k2 - Math.sqrt(discriminant)) / (2 * k1);

    return [t1, t2];
  }
}

/**
 * Compute lighting function;
 * @param {Vector} P
 * @param {Vector} N
 * @param {Vector} V - from object to camera;
 * @param {Number} s - specular;
 * @returns {Number}
 */
function computeLighting(P, N, V, s = -1) {
  let intensity = 0;

  for (let i = 0, l = scene.lights.length; i < l; i++) {
    let light = scene.lights[i];

    // @todo move calculates to Vector constructor ???
    if (light.typeIsAmbient()) {
      intensity += light.intensity();
    } else {
      /** @type {Vector} */
      let L;
      /** @type {Number} */
      let tMax;
      if (light.typeIsPoint()) {
        L = light.point().sub(P.toPoint()).toVector();
        tMax = 1;
      } else {
        L = light.point().toVector();
        tMax = Infinity;
      }

      // Shadow;
      const shadowData = closestIntersection(P.toPoint(), L, EPSILON, tMax);
      const shadowSphere = shadowData.sphere;
      const shadowT = shadowData.t;
      if (shadowSphere !== null) {
        continue;
      }

      // Diffuseness;
      let nDotL = N.dot(L);
      if (nDotL > 0) {
        intensity += light.intensity() * nDotL / (N.r() * L.r());
      }

      // Specularity;
      if (s !== -1) {
        let R = reflectRay(L, N);
        let rDotV = R.dot(V);
        if (rDotV > 0) {
          intensity += light.intensity() * Math.pow(rDotV / (R.r() * V.r()), s);
        }
      }
    }
  }

  return intensity;
}

/**
 * Reflect ray function;
 * @param {Vector} R - ray;
 * @param {Vector} N - normal;
 * @returns {Vector}
 */
function reflectRay(R, N) {
  // 2*N*dot(N, L) - L
  return N.mul(2 * N.dot(R)).sub(R);
}

/**
 * @typedef {Object} ClosestIntersection
 * @property {Sphere} sphere
 * @property {Number} t
 */

/**
 * @typedef {Object} Camera
 * @property {Point} position - Camera position
 */

/**
 * @typedef {Object} Scene
 * @property {Sphere[]} spheres
 * @property {Camera} camera
 * @property {Light[]} lights
 */
