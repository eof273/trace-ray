/* global Point */

/**
 * Light constructor;
 * @param {Number} intensity
 * @param {String} type
 * @param {Point} point - position for point or direction for directional;
 * @constructor
 */
function Light(intensity, type = Light.AMBIENT, point = new Point()) {
  // @todo static;
  const TYPES = [
    Light.AMBIENT,
    Light.POINT,
    Light.DIRECTIONAL
  ];

  let _intensity = intensity;
  let _type = checkType(type);
  let _point = Point.mustBePoint(point);

  // <editor-fold desc="getters/setters;">
  /**
   * Intensity of light;
   * @param {Number} [value]
   * @returns {Number}
   */
  this.intensity = function (value) {
    return value === undefined ? _intensity : _intensity = value;
  };

  /**
   * Type of light ('ambient', 'point' or 'directional');
   * @param {String} [value]
   * @returns {String}
   */
  this.type = function (value) {
    return value === undefined ? _type : _type = checkType(value);
  };

  /**
   * Position for point or direction for directional light type;
   * @param {Point} [value]
   * @returns {Point}
   */
  this.point = function (value) {
    return value === undefined ? _point : _point = Point.mustBePoint(value);
  };
  // </editor-fold>

  // <editor-fold desc="Check light types;">
  /**
   * Check type is ambient;
   * @return {Boolean}
   */
  this.typeIsAmbient = function () {
    return _type === Light.AMBIENT;
  };

  /**
   * Check type is point;
   * @return {Boolean}
   */
  this.typeIsPoint = function () {
    return _type === Light.POINT;
  };

  /**
   * Check type is directional;
   * @return {Boolean}
   */
  this.typeIsDirectional = function () {
    return _type === Light.DIRECTIONAL;
  };

  /**
   * Check type is a legal value;
   * @param {String} type
   * @return {String}
   */
  function checkType(type) {
    if (TYPES.indexOf(type) === -1) {
      throw Error('Light type must be one of that values: "ambient", "point" or "directional"!');
    }
    return type;
  }
  // </editor-fold>
}

// <editor-fold desc="Light type constants;" defaultstate="collapsed">
/**
 * @type {string}
 */
Object.defineProperty(Light, 'AMBIENT', {
  value: 'ambient',
  writable: false
});

/**
 * @type {string}
 */
Object.defineProperty(Light, 'POINT', {
  value: 'point',
  writable: false
});

/**
 * @type {string}
 */
Object.defineProperty(Light, 'DIRECTIONAL', {
  value: 'directional',
  writable: false
});
// </editor-fold>
